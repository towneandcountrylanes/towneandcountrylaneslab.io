---
title: Contact Us
subtitle: 
comments: false
---

Mail or in person:  
264 South Pine Street  
Burlington, Wisconsin 53105

Telephone: 262-763-7333  
Fax: 262-763-3231  
E-mail: info@towneandcountrylanes.com

## Employment Opportunities
![Towne & Country Lanes is proud to be an Equal Opportunity Employer!](https://towneandcountrylanes.com/images/eoev2.jpg)

**Be part of a family environment that has been in business over 50 years, and owned independently by the Draper family for better than 45 years.**

Come in and fill out an application.

We are not currently hiring. Thank you for your interest in Towne & Country Lanes.

For all positions, work is generally seasonal, running late August through the end of May.
